
Sega Rally 2 mdl format

# Structure
Header
Model[]
Node[]


# Model
 Material
 Vertex[]
 Face[]
 ModelPointer
 DrawOption


# Node
 Transform
 Relation


# Header
size 0x20.
| Offset | Type      | Name             | Description                                    |
|--------|-----------|------------------|------------------------------------------------|
| 0x00   | uint      | file_size        | size of this file(pallet?)                     |
| 0x04   | int       | header_size      | size of Header                                 |
| 0x08   | int       | rel0_offset      | Offset of Nodes[0].Relation                    |
| 0x0C   | int       | unk0x0C          |                                                |
| 0x10   | int       | unk0x10          |                                                |
| 0x14   | int       | unk0x14          |                                                |
| 0x18   | int       | unk0x18          |                                                |
| 0x1C   | int       | unk0x1C          |                                                |


# Model
## Material
| Offset | Type      | Name             | Description                                    |
|--------|-----------|------------------|------------------------------------------------|
| 0x00   | int       | color0           | mesh color 0                                   |
| 0x04   | int       | color1           | mesh color 1                                   |
| 0x08   | float     | unk_0x08         | unknown                                        |
| 0x0C   | float     | unk_0x0C         | unknown                                        |
| 0x10   | float     | unk_0x10         | unknown                                        |
| 0x14   | float     | unk_0x14         |                                                |
| 0x18   | float     | unk_0x18         |                                                |
| 0x1C   | float     | unk_0x1C         |                                                |


## Vertex
| Offset | Type      | Name             | Description                                    |
|--------|-----------|------------------|------------------------------------------------|
| 0x00   | Vector3   | pos              | vertex position                                |
| 0x0C   | Vector3   | nrm              | vertex normal                                  |
| 0x18   | Vector2   | uv               | UV-coord                                       |


### Vector3
| Offset | Type      | Name             | Description                                    |
|--------|-----------|------------------|------------------------------------------------|
| 0x00   | float     | x                | value-X                                        |
| 0x04   | float     | y                | value-Y                                        |
| 0x08   | float     | z                | value-Z                                        |


### Vector2
| Offset | Type      | Name             | Description                                    |
|--------|-----------|------------------|------------------------------------------------|
| 0x00   | float     | x                | value-X                                        |
| 0x04   | float     | y                | value-Y                                        |


## Face
array of short values.
| Offset | Type      | Name             | Description                                    |
|--------|-----------|------------------|------------------------------------------------|
| 0x00   | short     | vtx_idx          | Index of Vertex                                |


## ModelPointer
size 0x20.
| Offset | Type      | Name             | Description                                    |
|--------|-----------|------------------|------------------------------------------------|
| 0x00   | int       | vertexs_offset   | Offset of "Vertexs"                            |
| 0x04   | int       | faces_offs       | Offset of "Faces"                              |
| 0x08   | int       | material_offset  | Offset of "Material"                           |
| 0x0C   | int       | vertexs_length   | Length of "Vertexs"                            |
| 0x10   | int       | faces_length     | Length of "Faces"                              |
| 0x14   | int       | unk_0x14         |                                                |
| 0x18   | int       | unk_0x18         |                                                |
| 0x1C   | int       | unk_0x1C         |                                                |


## DrawOption
What is this...?
May ber Option for Draw.
size 0x80.
| Offset | Type      | Name             | Description                                    |
|--------|-----------|------------------|------------------------------------------------|
| 0x00   | int       | unk_0x00         | unknown                                        |
| 0x04   | int       | unk_0x04         | unknown                                        |
| 0x08   | float     | unk_0x08         |                                                |
| 0x0C   | float     | unk_0x0C         |                                                |
| 0x10   | float     | unk_0x10         |                                                |
| 0x14   | float     | unk_0x14         |                                                |
| 0x18   | int       | unk_0x18         |                                                |
| 0x1C   | int       | unk_0x1C         |                                                |



# Node
Contains "Transform" and "Relation".
size 0x80.

## Transform
| Offset | Type      | Name             | Description                                    |
|--------|-----------|------------------|------------------------------------------------|
| 0x00   | int       | modelPtr_offset  | Offset of "ModelPointer"                       |
| 0x04   | int       | drawOps_offset   | Offset of "DrawOption"                         |
| 0x08   | int       | unk_0x08         |                                                |
| 0x0C   | int       | unk_0x0C         |                                                |
| 0x10   | float     | unk_0x10         |                                                |
| 0x14   | float     | unk_0x14         |                                                |
| 0x18   | float     | unk_0x18         |                                                |
| 0x1C   | float     | unk_0x1C         |                                                |
| 0x20   | Vector3   | pos              | Position                                       |
| 0x2C   | int       | unk_0x2C         |                                                |
| 0x30   | int       | unk_0x30         |                                                |
| 0x34   | int       | unk_0x34         |                                                |
| 0x38   | int       | unk_0x38         |                                                |
| 0x3C   | int       | unk_0x3C         |                                                |
| 0x40   | Vector3   | scale            | scale                                          |
| 0x4C   | int       | unk_0x1C         |                                                |
| 0x50   | int       | unk_0x10         |                                                |
| 0x54   | int       | unk_0x14         |                                                |
| 0x58   | int       | unk_0x18         |                                                |
| 0x5C   | int       | unk_0x1C         |                                                |


## Relation
| Offset | Type      | Name             | Description                                    |
|--------|-----------|------------------|------------------------------------------------|
| 0x00   | int       | parent_offset    | Offset of "Node(child).Relation"               |
| 0x04   | int       | child_offset     | Offset of "Node(siblings).Relation"            |
| 0x08   | int       | unk_0x08         |                                                |
| 0x0C   | int       | unk_0x0C         |                                                |
| 0x10   | int       | unk_0x10         |                                                |
| 0x14   | int       | unk_0x14         |                                                |
| 0x18   | int       | unk_0x18         |                                                |
| 0x1C   | int       | unk_0x1C         |                                                |

