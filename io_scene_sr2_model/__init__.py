import bpy
import importlib

from . import import_sr2_model

bl_info= {
    "name": "Sega Rally 2 PC/DC 3D Model",
    "author": "chmcl95",
    "version": (0, 1, 0),
    "blender": (2, 80, 0),
    "location": "File > Import > Sega Rally 2",
    "description": "Trying to imports a Sega Rally 2 PC/DC version 3D model",
    "category": "Import",
}


if "bpy" in locals():
    import importlib
    if "import_sr2_model" in locals():
        importlib.reload(import_sr2_model)

from bpy.props import (
        StringProperty,
        )
from bpy_extras.io_utils import (
        ImportHelper,
        orientation_helper,
        axis_conversion
        )

@orientation_helper(axis_forward='Z', axis_up='-Y')
#Import Sega Rally 2 3D  Model
class IMPORT_SCENE_MT_SR2MODLE(bpy.types.Operator, ImportHelper):
    bl_idname = "import_scene.sr2_model"
    bl_label = "Import SR2 Model"
    bl_description = "Trying to imports a Sega Rally 2 PC/DC version 3D model"
    bl_options = {'REGISTER', 'UNDO'}

    filepath: StringProperty(
        name="File Path",
        description="Filepath used for importing the Sega Rally 2 PC/DC 3D Modle file",
        maxlen=1024)
    filter_glob: bpy.props.StringProperty(default="*.mdl", options={'HIDDEN'})

    def execute(self, context):
        keywords = self.as_keywords(ignore=("asxis_forward", \
                                    "axis_up", \
                                    "filter_glob",
                                    ))
        global_matrix = axis_conversion(from_forward = self.axis_forward, \
                                        from_up = self.axis_up, \
                                        ).to_4x4()
        import_sr2_model.load(self.filepath, global_matrix)
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        wm.fileselect_add(self)
        return {'RUNNING_MODAL'}


def menu_func_import(self, context):
    self.layout.operator(IMPORT_SCENE_MT_SR2MODLE.bl_idname, text="Sega Rally 2")


classes = (
    IMPORT_SCENE_MT_SR2MODLE,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)


def unregister():
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)

    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()
