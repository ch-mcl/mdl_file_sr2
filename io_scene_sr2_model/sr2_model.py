import array
import io
import struct


class Header:
    fmt = '<8I'
    
    def __init__(self, file):
        self.offset = file.tell()
        self.file_size = 0x00
        self.unk_0x04 = 0x00 # header size?
        self.relation_offset = 0x00
    
    def unpack(self, file):
        bytes = file.read(0x20)
        #bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        self.file_size = buff[0]
        self.unk_0x04 = buff[1]
        self.relation_offset = buff[2]


class Vertex:
    fmt = '<8f'
    
    def __init__(self, file):
        self.offset = file.tell()
        self.position = [ 0.0, 0.0, 0.0 ]
        self.normal = [ 0.0, 0.0, 0.0 ]
        self.uv = [ 0.0, 0.0 ]
    
    def unpack(self, file):
        bytes = file.read(0x20)
        #bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        self.position = [ buff[0], buff[1], buff[2] ]
        self.normal = [ buff[3], buff[4], buff[5] ]
        self.uv = [ buff[6], buff[7] ]


class Mesh:
    fmt = '<8I'
    fmt_face = '<{0}h'
    
    def __init__(self, file):
        self.offset = file.tell()
        self.vertex_offset = 0x00
        self.face_offset = 0x00
        self.material_offset = 0x00
        self.vertex_count = 0x00
        self.face_count = 0x00
        self.maerial_count = 0x00
        self.unk_0x18 = 0x00
        self.unk_0x1C = 0x00
        self.vertexs = []
        self.faces = []

    def unpack(self, file):
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        self.vertex_offset = buff[0]
        #print('vertex offset : {0:#X}'.format(buff[0]))
        self.face_offset = buff[1]
        self.material_offset = buff[2]
        self.vertex_count = buff[3]
        self.face_count = buff[4]
        self.maerial_count = buff[5]
        self.unk_0x18 = buff[6]
        self.unk_0x1C = buff[7]
        
        if (self.vertex_offset > 0 and self.vertex_offset != 0xffffff):
            #Vertex
            file.seek(self.vertex_offset)
            for i in range (0, self.vertex_count):
                vertex = Vertex(file)
                vertex.unpack(file)
                self.vertexs.append(vertex)
            #Face
            file.seek(self.face_offset)
            #print('Face offset: {0:#X}'.format(self.face_offset))
            fmt_faces = self.fmt_face.format(self.face_count)
            bytes = file.read(struct.calcsize(fmt_faces))
            self.faces = struct.unpack_from(fmt_faces, bytes, 0)
            #print('Faces')
            #print(self.faces)
            

class Node:
    fmt = '<4I'+'4f'+'4f'+'4f'+'3f20x8I'
    #  4I  4f   4f    4f     3f     20x    8I'
    # 0-3, 4-7, 8-11, 12-15, 16-18, 19-29, 30-38
    
    def __init__(self, file):
        self.offset = file.tell()
        self.mesh = Mesh(file) #TODO: rename to "mesh"
        self.mesh_offset = 0x00
        self.draw_ops_offset = 0x00
        self.unk_0x08 = 0x00
        self.unk_0x0C = 0x00
        self.unk_0x2C = 0x00
        self.unk_0x30 = 0x00
        self.position = [ 0, 0, 0 ]
        self.scale = [ 0, 0, 0 ]
        
    def unpack(self, file):
        bytes = file.read(0x80)
        #bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        self.mesh_offset = buff[0]
        self.draw_ops_offset = buff[1]
        self.unk_0x08 = buff[2]
        self.unk_0x0C = buff[3]
        #4, 5, 6, 7
        self.position = [ buff[8], buff[9], buff[10] ]
        self.unk_0x2C = buff[11]
        self.unk_0x30 = buff[12]
        # 13, 14, 15
        self.scale = [ buff[16], buff[17], buff[18] ]
        
        #Mesh
        file.seek(self.mesh_offset)
        #print('-- Mesh --')
        #print('Mesh: {0:#X}'.format(file.tell()))
        self.mesh.unpack(file)
        

class Mdl:
    def __init__(self, file):
        self.header = Header(file)
        self.nodes = []
    
    def unpack(self, file):
        self.header.unpack(file)
        #print('File size: {0:#X}'.format(self.header.file_size))
        #print('Node offset: {0:#X}'.format(self.header.relation_offset))
            # Go to EOF
        file.seek(0x0, io.SEEK_END)
        i = 0
        #while (True):
        for loop_i in range(9):
            if i > 10:
                break
            file.seek(-0x80, io.SEEK_CUR)
            node = Node(file)
            node.unpack(file)
            self.nodes.append(node)
            if (node.unk_0x0C == 0xFFFFFFFF \
            and node.unk_0x2C == 0x00 \
            and node.unk_0x30 == 0x00):
                break
            file.seek(node.offset)
            #print('File Position: {0:#X}'.format(file.tell()))
            i = i + 1
            
        #print('File Position: {0:#X}'.format(file.tell()))
        #print('Nodes length: {0:#X}'.format(len(self.nodes)))