import array
import io
import mathutils

import bmesh
import bpy

from .sr2_model import Mdl, Node, Mesh


#Storage Mesh datas
class Mesh_data:
    def __init__(self):
        self.normals = [] #Collect for "Custom Split Normals"
        #self.color0s = []
        self.uvs = []


def generate_mesh(node: Node, mdl_mesh: Mesh, idx: int, global_matrix: mathutils.Matrix):
    mesh_name = 'mesh_{0:04}'.format(idx)
    bl_mesh = bpy.data.meshes.new(mesh_name)
    bl_obj = bpy.data.objects.new(mesh_name, bl_mesh)

    bpy.context.collection.objects.link(bl_obj)
    bpy.context.view_layer.objects.active = bl_obj
    bl_obj.select_set(True)
    bl_mesh = bpy.context.object.data
    bm = bmesh.new()
    # Position
    bl_obj.location = node.position
    # Scale
    bl_obj.scale = node.scale

    mesh_data = Mesh_data() # Store Normals, UVs, VertexColors, etc
    
    #vertex
    vtxs = []
    for vtx in mdl_mesh.vertexs:
        v = bm.verts.new(vtx.position)
        vtxs.append(v)
        mesh_data.normals.append(vtx.normal)
        mesh_data.uvs.append(vtx.uv)
        
    #face
    for i in range(0, len(mdl_mesh.faces), 3):
        v0 = vtxs[mdl_mesh.faces[i]]
        v1 = vtxs[mdl_mesh.faces[i+1]]
        v2 = vtxs[mdl_mesh.faces[i+2]]
        bm.faces.new((v0, v1, v2))

    bm.to_mesh(bl_mesh)  
    bm.free()
    
    #uv
    channel_name = "uv0"
    try:
        bl_mesh.uv_layers[channel_name].data
    except:
        #Generate UV
        bl_mesh.uv_layers.new(name = channel_name)
    for i, loop in enumerate(bl_mesh.loops):
        bl_mesh.uv_layers[channel_name].data[i].uv = mesh_data.uvs[loop.vertex_index]
    
    # apply normal
    bl_mesh.polygons.foreach_set("use_smooth", [True] * len(bl_mesh.polygons))
    bl_mesh.use_auto_smooth = True
    bl_mesh.normals_split_custom_set_from_vertices(mesh_data.normals)

    #Object Mode
    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
    inv_mtx = mathutils.Matrix.inverted(global_matrix)
    bpy.context.object.matrix_world = inv_mtx @ bpy.context.object.matrix_world


def load(filepath: str, global_matrix: mathutils.Matrix):
    # open files
    with open(filepath, 'rb') as file:
        # Parse
        mdl = Mdl(file)
        mdl.unpack(file)

        
        # Generate Mesh
        for idx, node in enumerate(mdl.nodes):
            mdl_mesh = node.mesh
            generate_mesh(node, mdl_mesh, idx, global_matrix)
    
    file.close()